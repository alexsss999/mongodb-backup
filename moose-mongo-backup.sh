#!/bin/bash
#
# ***********************************************************
# File:          moose-mongo-backup.sh
# Author:        Safonov A.M.
# Main function: backup mongo-bases in docker
#                
# 
# Usage:         Run script
#
# ver. 0.2
# last update:   24.12.2017
# License:       GPL v.2
# ***********************************************************
# server: ANY

# 1. Definion
PING="/bin/ping"
CP="/bin/cp"
MV="/bin/mv"
RM="/bin/rm"
MKDIR="/bin/mkdir"
MOUNT="/bin/mount"
UMOUNT="/bin/umount"
CHOWN="/bin/chown"
CHMOD="/bin/chmod"

SCRIPTS_DIR="/"
BACKUP_DIR="/mongo_backup"

# log-files
WARNING_LOG="bases_warning.log"
BACKUP_LOG="bases_backup.log"

#SERVER_NAME="srv-vm1.icons8.com"

# time definion
YEAR=`date +%Y` # year 4-digit
MNTH=`date +%m` # mounth of year 1-12
DAT=`date +%d`  # day of mounth 1-31
HOUR=`date +%H` # hour of day 0-24
MINUTES=`date +%M` # hour of day 0-24
FULLDATE=`date +%c` # full date

#DEL_AFTER_DAYS="28" # 4 week
DEL_AFTER_DAYS="5"

OLD_YEAR=`date -d "-${DEL_AFTER_DAYS} day" +%Y` # year 4-digit
OLD_MNTH=`date -d "-${DEL_AFTER_DAYS} day" +%m` # mounth of year 1-12
OLD_DAT=`date -d "-${DEL_AFTER_DAYS} day" +%d`  # day of mounth 1-31
# ----------------------------------------------------------

# 2. Backup /home

# Delete OLD backup folder

if [ -d $BACKUP_DIR/archive/${OLD_YEAR}${OLD_MNTH}${OLD_DAT} ];
    then
	#echo "Delete folder $BACKUP_DIR/${OLD_YEAR}${OLD_MNTH}${OLD_DAT}"
	$RM -rf $BACKUP_DIR/archive/${OLD_YEAR}${OLD_MNTH}${OLD_DAT}
fi


# Make backup folder

if [ ! -d $BACKUP_DIR/archive/${YEAR}${MNTH}${DAT} ];
    then
	#echo "Create folder $BACKUP_DIR/${YEAR}${MNTH}${DAT}"
	$MKDIR -p $BACKUP_DIR/archive/${YEAR}${MNTH}${DAT}
fi

# 2. Архивирование баз MongoBD
$RM -rf $BACKUP_DIR/raw/*

/usr/bin/mongodump --host mongodb -o $BACKUP_DIR/raw/
cd $BACKUP_DIR/raw
/bin/tar --sparse -czf $BACKUP_DIR/archive/${YEAR}${MNTH}${DAT}/moose-mongo-${YEAR}${MNTH}${DAT}_${HOUR}-${MINUTES}.tar.gz *
cd /


exit 0
